const Blog = require("../models/blog.model");

const getBlogs = async (_req, res) => {
  try {
    const blogs = await Blog.find();
    return res.json(blogs);
  } catch (err) {
    console.log({ err });
    res.send("could not fetch blogs");
  }
};

const createBlog = async (req, res) => {
  try {
    const blog = await new Blog(req.body).save();
    res.status(201).json(blog);
  } catch (err) {
    console.log({ err });
    res.send("could not create blog");
  }
};

module.exports = {
  createBlog,
  getBlogs,
};
