const express = require("express");
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const { MONGODB_URI, isDev } = require("./utils/config");

mongoose.connect(MONGODB_URI, () => {
  if (isDev) console.log("connected to mongodb");
});

app.use(cors());
app.use(express.json());

app.use("/api/blogs", require("./controllers/blog"));

module.exports = app;
