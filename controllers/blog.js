const { Router } = require("express");
const { getBlogs, createBlog } = require("../component/blog");
const router = Router();

router.get("/", getBlogs);

router.post("/", createBlog);

module.exports = router;
