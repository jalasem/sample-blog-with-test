require("dotenv").config();

module.exports = {
  PORT: process.env.PORT || 3000,
  isDev: process.env.NODE_ENV === "development",
  MONGODB_URI:
    process.env.NODE_ENV === "test"
      ? process.env.TEST_DB_URI
      : process.env.MONGODB_URI,
};
